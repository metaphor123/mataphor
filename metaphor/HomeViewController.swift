//
//  HomeViewController.swift
//  metaphor
//
//  Created by Pranav Bhandari on 6/22/16.
//  Copyright © 2016 metaphor. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let home_fname = UILabel(frame: CGRectMake(10,20,250,20))
        print("ppppppppp ",projects.count)
        home_fname.text = "Welcome," + fname + " " + lname
        self.view.addSubview(home_fname)
        
        
        
        // Do any additional setup after loading the view.
        
        //adding uitableview controller
        let controller = storyboard!.instantiateViewControllerWithIdentifier("TableView")
        addChildViewController(controller)
        controller.view.frame = CGRectMake(50, 100, 400, 400)
        view.addSubview(controller.view)
        controller.didMoveToParentViewController(self)
        
        
        //adding add new project button
        // MARK: - Add new project
        // let add_proj = UIButton(frame: CGRectMake(width - width/10,height - 2*height/10,width/10,height/10))
        let add_proj = UIButton(frame: CGRectMake(300,150,100,20))
        add_proj.backgroundColor = .greenColor()
        add_proj.setTitle("Add new project", forState: .Normal)
        add_proj.addTarget(self, action: #selector(buttonAction), forControlEvents: .TouchUpInside)
        self.view.addSubview(add_proj)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buttonAction(sender: UIButton!) {
        var popoverContent = self.storyboard?.instantiateViewControllerWithIdentifier("NewProj") as! AddNewProjectViewController
        
        popoverContent.modalPresentationStyle = .Popover
        var popover = popoverContent.popoverPresentationController
        
        if let popover = popoverContent.popoverPresentationController {
            
            let viewForSource = sender as! UIView
            popover.sourceView = viewForSource
            
            // the position of the popover where it's showed
            popover.sourceRect = viewForSource.bounds
            
            // the size you want to display
            popoverContent.preferredContentSize = CGSizeMake(width,height)
            //popover.delegate = self
        }
        
        self.presentViewController(popoverContent, animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
