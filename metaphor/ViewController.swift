//
//  ViewController.swift
//  metaphor
//
//  Created by Ankit Mahale on 6/20/16.
//  Copyright © 2016 metaphor. All rights reserved.
//

import UIKit
var font_name = "Bangla Sangam MN"
var bounds = UIScreen.mainScreen().bounds
var width = bounds.size.width
var height = bounds.size.height
var login_emailField = UITextField()
var login_passField = UITextField()
var fname = String(),lname = String()
var allow_login = false,cnt = 0
var project_names = [String]()
var projects = NSArray();




class ViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .blackColor()
        self.email()
        self.password()
        self.login_btn()
        self.registerBtn()
        
        
        
        
        // MARK: - Logo
        let logo = UILabel(frame: CGRectMake(view.frame.size.width/2,30,width,50))
        logo.center = CGPointMake(view.frame.size.width/2, 30)
        logo.textAlignment = .Center
        logo.text = "METAPHOR"
        logo.textColor = .whiteColor()
        logo.font = UIFont.init(name: font_name, size: 45.0)
        self.view.addSubview(logo)
        
        let log_in = UILabel(frame: CGRectMake(0,100,width,50))
        log_in.center = CGPointMake(view.frame.size.width/2, 100)
        log_in.textAlignment = .Center
        log_in.text = "Log In"
        log_in.textColor = .whiteColor()
        log_in.font = UIFont.init(name: font_name, size: 30)
        self.view.addSubview(log_in)
        
        // MARK: - forgot password?
        let forgot_pwd_text = UIButton(frame: CGRectMake(0,height/16 - 1.5*height/16,width,50))
        forgot_pwd_text.center = CGPointMake(view.frame.size.width/2, height - 1.5*height/16)
        forgot_pwd_text.setTitle("Forgot Password?", forState: .Normal)
        forgot_pwd_text.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.view.addSubview(forgot_pwd_text)
        forgot_pwd_text.titleLabel?.font = UIFont.init(name: font_name, size: 25)
        forgot_pwd_text.addTarget(self, action: #selector(login_clicked(_:)), forControlEvents: .TouchUpInside)
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func email()
    {
        //let emailField = UITextField(frame: CGRectMake(10,100,400,50))
        login_emailField = UITextField(frame: CGRectMake(0,4*height/16,width,1.5*height/16))
        login_emailField.attributedPlaceholder =  NSAttributedString(string:"Email", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        login_emailField.textAlignment = .Center
        //login_emailField.textColor = UIColor(red: 255/255, green: 240/255, blue: 0/255, alpha: 0.33)
        login_emailField.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        
        login_emailField.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        self.view.addSubview(login_emailField)
        login_emailField.font = UIFont.init(name: font_name, size: 25)
        //login_emailField.layer.cornerRadius = 8
        //login_emailField.layer.borderWidth = 2
        
    }
    func password()
    {
        
        login_passField = UITextField(frame: CGRectMake(0,5.75*height/16,width,1.5*height/16))
        login_passField.textAlignment = .Center
        login_passField.attributedPlaceholder =  NSAttributedString(string:"Password", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        login_passField.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        login_passField.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        self.view.addSubview(login_passField)
        login_passField.font = UIFont.init(name: font_name, size: 25)
        
        //login_passField.layer.cornerRadius = 8
        //login_passField.layer.borderWidth = 2
        
    }
    
    func login_btn()
    {
        
        let login_btn = UIButton(frame: CGRectMake(0,8*height/16,width,1.25*height/16))
        login_btn.backgroundColor = UIColor(red: 28/255, green: 81/255, blue: 167/255, alpha: 1)
        login_btn.setTitle("Login", forState: .Normal)
        login_btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.view.addSubview(login_btn)
        login_btn.titleLabel?.font = UIFont.init(name: font_name, size: 25)
        login_btn.addTarget(self, action: #selector(login_clicked(_:)), forControlEvents: .TouchUpInside)
        // login_btn.titleLabel?.textColor = UIColor.whiteColor()
    }
    
    
    func registerBtn()
    {
        
        let registerbtn = UIButton(frame: CGRectMake(6*width/10,height/2,width/3,height/16))
        registerbtn.setTitle("Register Now", forState: .Normal)
        //self.view.addSubview(registerbtn)
        registerbtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        registerbtn.backgroundColor = .blueColor()
        
    }
    
    
    func login_clicked(sender: UIButton!) {
        //        let postEndpoint: String = "https://localhost:3000/users?fname=pranav"
        //        let session = NSURLSession.sharedSession()
        //        let url = NSURL(string: postEndpoint)!
        //
        //        // Make the POST call and handle it in a completion handler
        //        session.dataTaskWithURL(url, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
        //            // Make sure we get an OK response
        //            guard let realResponse = response as? NSHTTPURLResponse where
        //                realResponse.statusCode == 200 else {
        //                    print("Sorry Not a 200 response")
        //                    return
        //            }
        //
        //            // Read the JSON
        //            do {
        //                if let ipString = NSString(data:data!, encoding: NSUTF8StringEncoding) {
        //                    // Print what we got from the call
        //                    print(ipString)
        //
        //                    // Parse the JSON to get the IP
        //                    let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
        //                    let origin = jsonDictionary["origin"] as! String
        //
        //                    // Update the label
        //                    self.performSelectorOnMainThread("updateIPLabel:", withObject: origin, waitUntilDone: false)
        //                }
        //            } catch {
        //                print("bad things happened")
        //            }
        //        }).resume()
        
        //let requestURL: NSURL = NSURL(string: "http://www.learnswiftonline.com/Samples/subway.json")!
        
        //let URLString = "http://localhost:3000/login?email="+login_emailField.text!+"&password="+login_passField.text!
        
        
        let URLString = "http://localhost:3000/login?email=1@1.c&password=1"
        let requestURL: NSURL = NSURL(string: URLString)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            if (statusCode == 200) {
                print("Everyone is fine, file downloaded successfully.")
                
                do{
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)
                    
                    if let stations = json["message"] as? [[String: AnyObject]] {
                        if(stations.count > 0)
                        {
                            allow_login = true
                        }
                        for station in stations {
                            
                            if let fn = station["fname"] as? String {
                                fname = fn
                                if let ln = station["lname"] as? String {
                                    lname = ln
                                    // if let year = station["buildYear"] as? String {
                                    print(stations.count,lname,fname,allow_login)
                                    projects = station["projects"] as! NSArray
                                    print(projects)
                                    
                                    // if projects = station["projects"] as! NSArray {
                                    // project_names = projects[0]["project_name"] as! String
                                    
                                    //print(stations.count,lname,name,projects[0]["project_name"])
                                    //  }
                                    
                                }
                                
                            }
                        }
                        
                        
                        
                    }
                    
                }catch {
                    print("Error with Json: \(error)")
                }
                
                
                
                
                
            }
        }
        task.resume()
        self.nextview()
        
        
        
    }
    
    func nextview() {
        if(allow_login)
        {
            print("ok m inside ")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("HomePage") as! HomePageViewController
            self.presentViewController(nextViewController, animated:true, completion:nil)
        }
    }
}

