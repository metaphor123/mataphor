//
//  HomePageViewController.swift
//  metaphor
//
//  Created by Pranav Bhandari on 6/30/16.
//  Copyright © 2016 metaphor. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITabBarDelegate {
    //@IBOutlet var profileTabBar : UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        // profileTabBar.frame = CGRectMake(0, height, width, height-16)
        //  profileTabBar.frame.origin.y = height - height/16
        //profileTabBar.center = CGPointMake(100, 100)
        
        
        
        // MARK: - adding tab bar
        
        let tb: UITabBar = UITabBar(frame: CGRectMake(0, height-height/16, width, height/16))
        tb.backgroundColor = .blackColor()
        let tbi1: UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "home"), tag: 0)
        let tbi2: UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "search-icon"), tag: 1)
        let tbi3: UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "camera-icon"), tag: 3)
        let tbi4: UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "user_icon"), tag: 4)
        tb.items = [tbi1, tbi2, tbi3, tbi4]
        tb.delegate = self
        // Delegate of your UITabBar is your detailViewController
        self.view.addSubview(tb)
        
        // Do any additional setup after loading the view.
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        let labelHeight: CGFloat = 20.0
        let lab: UILabel = UILabel(frame: CGRectMake(10, 10, width - 2 , labelHeight))
        lab.text = "\(item.title) with tag \(Int(item.tag))"
        lab.font = UIFont.systemFontOfSize(12.0)
        self.view.addSubview(lab)
        // labelYCurrentPosition += labelHeight
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //        return 0.1
    //    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print("rowssssss",projects.count)
        return projects.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LabelCell", forIndexPath: indexPath)
        
        // Configure the cell...
        // cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        print(".........",projects[indexPath.row]["project_name"])
        cell.textLabel?.text = projects[indexPath.row]["project_name"] as? String
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        print("selected cell",projects[indexPath.row]["project_name"])
    }
    //    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        return "METAPHOR"
    //    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRectMake(0, 0, width, 200)) //set these values as necessary
        returnedView.backgroundColor = UIColor.blackColor()
        
        let label = UILabel(frame: CGRectMake(20, 0, width, height/16))
        label.text = "METAPHOR"
        label.textColor = .whiteColor()
        returnedView.addSubview(label)
        
        let imageViewGame = UIImageView(frame: CGRectMake(width - 2*width/16, 2.5, 2*width/16, 0.9*height/16));
        //var image = UIImage(named: "Games.png");
        let image = UIImage(data: NSData(contentsOfURL: NSURL(string:"https://media-cdn.tripadvisor.com/media/photo-s/03/9b/2d/f2/new-york-city.jpg")!)!)
        imageViewGame.image = image;
        returnedView.addSubview(imageViewGame)
        
        // MARK: - Making image circle
        imageViewGame.layer.borderWidth = 1
        imageViewGame.layer.masksToBounds = false
        imageViewGame.layer.borderColor = UIColor.blackColor().CGColor
        imageViewGame.layer.cornerRadius = imageViewGame.frame.height/2
        imageViewGame.clipsToBounds = true
        
        return returnedView
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return height/16
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
