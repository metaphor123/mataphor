//
//  AddNewProjectViewController.swift
//  metaphor
//
//  Created by Pranav Bhandari on 6/27/16.
//  Copyright © 2016 metaphor. All rights reserved.
//

import UIKit

class AddNewProjectViewController: UIViewController {
var new_proj_inp = UITextField(),new_proj_desc_inp = UITextView(), new_proj_team_inp = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.new_proj_name_lbl()
        self.new_proj_name_input()
        self.new_proj_desc_lbl()
        self.new_proj_desc_input()
        self.new_proj_team_lbl()
        self.new_proj_team_input()
        self.new_proj_img_lbl()
        self.cancel_button()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func new_proj_name_lbl()
    {
        let new_proj_lbl = UILabel(frame: CGRectMake(width/10,height/15,3*width/10,height/15))
        print("width----",width,height)
        new_proj_lbl.text = "Project Name: "
        self.view.addSubview(new_proj_lbl)
    }
    
    func new_proj_name_input()
    {
        new_proj_inp = UITextField(frame: CGRectMake(4*width/10,height/15,6*width/10,0.9*height/15))
        new_proj_inp.placeholder = "Enter Project Name"
        self.view.addSubview(new_proj_inp)
        new_proj_inp.font = UIFont.init(name: "Futura-Medium", size: 25)
        new_proj_inp.layer.cornerRadius = 8
        new_proj_inp.layer.borderWidth = 2
        
        
    }
    
    func new_proj_desc_lbl()
    {
        let new_proj_desc_lbl = UILabel(frame: CGRectMake(width/10,2*height/15,3*width/10,2*height/15))
        new_proj_desc_lbl.text = "Project Desc: "
        self.view.addSubview(new_proj_desc_lbl)
    }
    
    func new_proj_desc_input()
    {
        new_proj_desc_inp = UITextView(frame: CGRectMake(4*width/10,2*height/15,6*width/10,1.9*height/15))
       // new_proj_desc_inp.placeholder = "Enter Names"
        self.view.addSubview(new_proj_desc_inp)
        new_proj_desc_inp.font = UIFont.init(name: "Futura-Medium", size: 25)
        new_proj_desc_inp.layer.cornerRadius = 8
        new_proj_desc_inp.layer.borderWidth = 2
        
    }
    
    func new_proj_team_lbl()
    {
        let new_proj_team_lbl = UILabel(frame: CGRectMake(width/10,4*height/15,3*width/10,height/15))
        new_proj_team_lbl.text = "Project Team: "
        self.view.addSubview(new_proj_team_lbl)
    }
    func new_proj_team_input()
    {
        new_proj_team_inp = UITextField(frame: CGRectMake(4*width/10,4*height/15,6*width/10,0.9*height/15))
        new_proj_team_inp.placeholder = "Enter Team Members"
        self.view.addSubview(new_proj_team_inp)
        new_proj_team_inp.font = UIFont.init(name: "Futura-Medium", size: 25)
        new_proj_team_inp.layer.cornerRadius = 8
        new_proj_team_inp.layer.borderWidth = 2
        
        
    }
    
    func new_proj_img_lbl()
    {
        let new_proj_img_lbl = UILabel(frame: CGRectMake(width/10,5*height/15,3*width/10,height/15))
        new_proj_img_lbl.text = "Project Image: "
        self.view.addSubview(new_proj_img_lbl)
    }
    
    func cancel_button()
    {
        let cancel_btn = UIButton(frame: CGRectMake(width/10,6*height/15,3*width/10,height/15))
        cancel_btn.backgroundColor = .greenColor()
        cancel_btn.setTitle("Cancel", forState: .Normal)
        cancel_btn.addTarget(self, action: #selector(cancel_btn_func), forControlEvents: .TouchUpInside)
        self.view.addSubview(cancel_btn)
    }
    
    func cancel_btn_func()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("Home") as! HomeViewController
        self.presentViewController(nextViewController, animated:true, completion:nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
