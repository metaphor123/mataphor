//
//  StartPageViewController.swift
//  metaphor
//
//  Created by Pranav Bhandari on 7/1/16.
//  Copyright © 2016 metaphor. All rights reserved.
//

import UIKit

class StartPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .blackColor()
        
        // MARK: - Logo
        let logo = UILabel(frame: CGRectMake(view.frame.size.width/2,30,width,50))
        logo.center = CGPointMake(view.frame.size.width/2, 30)
        logo.textAlignment = .Center
        logo.text = "METAPHOR"
        logo.textColor = .whiteColor()
        logo.font = UIFont.init(name: font_name, size: 45.0)
        self.view.addSubview(logo)
        
        
        // MARK: - Login Button
        let login_btn = UIButton(frame: CGRectMake(0,height - 1.5*height/16,0.8*width/2,1.25*height/16))
        login_btn.backgroundColor = UIColor(red: 28/255, green: 81/255, blue: 167/255, alpha: 1)
        login_btn.center = CGPointMake(width/4, height-1.5*height/16)
        login_btn.setTitle("Log in", forState: .Normal)
        login_btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.view.addSubview(login_btn)
        login_btn.titleLabel?.font = UIFont.init(name: font_name, size: 25)
        login_btn.addTarget(self, action: #selector(login_clicked(_:)), forControlEvents: .TouchUpInside)
        // Do any additional setup after loading the view.
        
        
        // MARK: - Register Button
        let reg_btn = UIButton(frame: CGRectMake(0,0,0.8*width/2,1.25*height/16))
        reg_btn.center = CGPointMake(width/2 + width/4, height - 1.5*height/16)
        reg_btn.backgroundColor = UIColor(red: 28/255, green: 81/255, blue: 167/255, alpha: 1)
        reg_btn.setTitle("Register", forState: .Normal)
        reg_btn.setTitleColor(.whiteColor(), forState: .Normal)
        reg_btn.titleLabel?.font = UIFont.init(name: font_name, size:25)
        reg_btn.addTarget(self, action: #selector(register_clicked(_:)), forControlEvents: .TouchUpInside)
        self.view.addSubview(reg_btn)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func register_clicked(sender: UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("reg_1") as! Register_pg1_ViewController
        self.presentViewController(nextViewController, animated:true, completion:nil)
    }
    
    func login_clicked(sender: UIButton!)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("Login") as! ViewController
        self.presentViewController(nextViewController, animated:true, completion:nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
