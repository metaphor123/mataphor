//
//  Register_pg1_ViewController.swift
//  metaphor
//
//  Created by Pranav Bhandari on 7/1/16.
//  Copyright © 2016 metaphor. All rights reserved.
//

import UIKit
var reg1_fname = UITextField(),reg1_lname = UITextField(), reg1_email = UITextField()
var reg1_pwd = UITextField(), reg1_cpwd = UITextField(), reg1_passion = UITextField()
var addContact : ViewController = ViewController()
class Register_pg1_ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .blackColor()
        
        self.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        // Cover Vertical is necessary for CurrentContext
        self.modalPresentationStyle = .CurrentContext
        // Display on top of    current UIView
        self.presentViewController(ViewController(), animated: true, completion: nil)

        
        // MARK: - Logo
        let logo = UILabel(frame: CGRectMake(view.frame.size.width/2,30,width,50))
        logo.center = CGPointMake(view.frame.size.width/2, 30)
        logo.textAlignment = .Center
        logo.text = "METAPHOR"
        logo.textColor = .whiteColor()
        logo.font = UIFont.init(name: font_name, size: 45.0)
        self.view.addSubview(logo)
        
        let log_in = UILabel(frame: CGRectMake(0,100,width,50))
        log_in.center = CGPointMake(view.frame.size.width/2, 100)
        log_in.textAlignment = .Center
        log_in.text = "Register"
        log_in.textColor = .whiteColor()
        log_in.font = UIFont.init(name: font_name, size: 30)
        self.view.addSubview(log_in)
        
        // Do any additional setup after loading the view.
        
        //MARK: - Adding TextFields
        reg1_fname = UITextField(frame: CGRectMake(0,3*height/16,width,1.5*height/16))
        reg1_fname.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        reg1_fname.attributedPlaceholder =  NSAttributedString(string:"First Name", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        reg1_fname.textAlignment = .Center
        reg1_fname.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        reg1_fname.font = UIFont.init(name: font_name, size: 25)
        self.view.addSubview(reg1_fname)
        
        reg1_lname = UITextField(frame: CGRectMake(0,4.7*height/16,width,1.5*height/16))
        reg1_lname.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        reg1_lname.attributedPlaceholder =  NSAttributedString(string:"Last Name", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        reg1_lname.textAlignment = .Center
        reg1_lname.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        reg1_lname.font = UIFont.init(name: font_name, size: 25)
        self.view.addSubview(reg1_lname)
        
        reg1_email = UITextField(frame: CGRectMake(0,6.4*height/16,width,1.5*height/16))
        reg1_email.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        reg1_email.attributedPlaceholder =  NSAttributedString(string:"Email", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        reg1_email.textAlignment = .Center
        reg1_email.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        reg1_email.font = UIFont.init(name: font_name, size: 25)
        self.view.addSubview(reg1_email)
        
        reg1_pwd = UITextField(frame: CGRectMake(0,8.1*height/16,width,1.5*height/16))
        reg1_pwd.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        reg1_pwd.attributedPlaceholder =  NSAttributedString(string:"Password", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        reg1_pwd.textAlignment = .Center
        reg1_pwd.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        reg1_pwd.font = UIFont.init(name: font_name, size: 25)
        self.view.addSubview(reg1_pwd)
        
        reg1_cpwd = UITextField(frame: CGRectMake(0,9.8*height/16,width,1.5*height/16))
        reg1_cpwd.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        reg1_cpwd.attributedPlaceholder =  NSAttributedString(string:"Confirm Password", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        reg1_cpwd.textAlignment = .Center
        reg1_cpwd.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        reg1_cpwd.font = UIFont.init(name: font_name, size: 25)
        self.view.addSubview(reg1_cpwd)
        
        reg1_passion = UITextField(frame: CGRectMake(0,11.5*height/16,width,1.5*height/16))
        reg1_passion.backgroundColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
        reg1_passion.attributedPlaceholder =  NSAttributedString(string:"Passion", attributes: [NSForegroundColorAttributeName: UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)])
        reg1_passion.textAlignment = .Center
        reg1_passion.textColor = UIColor.colorWithAlphaComponent(UIColor.whiteColor())(0.33)
        reg1_passion.font = UIFont.init(name: font_name, size: 25)
        self.view.addSubview(reg1_passion)
        
        let next = UIButton(frame: CGRectMake(0,height-1.5*height/16,width,1.25*height/16))
        next.backgroundColor = UIColor(red: 28/255, green: 81/255, blue: 167/255, alpha: 1)
        next.setTitle("Next", forState: .Normal)
        next.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.view.addSubview(next)
        next.titleLabel?.font = UIFont.init(name: font_name, size: 25)
        next.addTarget(self, action: #selector(next_clicked(_:)), forControlEvents: .TouchUpInside)
    }
    
    func next_clicked(sender: UIButton)
    {
        reg1_fname.layer.borderColor = UIColor.clearColor().CGColor
        reg1_lname.layer.borderColor = UIColor.clearColor().CGColor
        reg1_email.layer.borderColor = UIColor.clearColor().CGColor
        reg1_pwd.layer.borderColor = UIColor.clearColor().CGColor
        reg1_cpwd.layer.borderColor = UIColor.clearColor().CGColor
        reg1_passion.layer.borderColor = UIColor.clearColor().CGColor
        if(reg1_fname.text != "")
        {
            print("border")
            if(reg1_lname.text != "")
            {
                if(reg1_email.text != "")
                {
                    if(reg1_pwd.text != "")
                    {
                        if(reg1_pwd.text == reg1_cpwd.text)
                        {
                            if(reg1_passion.text != "")
                            {
                                
                            }
                            else
                            {
                                reg1_passion.layer.borderWidth = 3
                                reg1_passion.layer.borderColor = UIColor.redColor().CGColor
                            }
                        }
                        else
                        {
                            reg1_cpwd.layer.borderWidth = 3
                            reg1_cpwd.layer.borderColor = UIColor.redColor().CGColor
                        }
                    }
                    else
                    {
                        reg1_pwd.layer.borderColor = UIColor.redColor().CGColor
                        reg1_pwd.layer.borderWidth = 3
                    }
                }
                else
                {
                    reg1_email.layer.borderWidth = 3
                    reg1_email.layer.borderColor = UIColor.redColor().CGColor
                }
            }
            else
            {
                print("border1")
                reg1_lname.layer.borderWidth = 3
                reg1_lname.layer.borderColor = UIColor.redColor().CGColor
            }
        }
        else
        {
            reg1_fname.layer.borderWidth = 3
            reg1_fname.layer.borderColor = UIColor.redColor().CGColor
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
